# Turismo Api

## Introduction

> Este es una api creada para citios turísticos,  hoteleria y restaurantes que servira en yucatan

## Requerimientos
> Conda
> Se basa en Django 2.1
> djangorestframework      3.8.2
> etc
## Installation

> Requerido instalar [Conda] 
crear la carpeta media
|carpeta|ubicabion|
|-------|----------|
|media/ |principal|


instalar las dependencias de conda ya instalados se propone, verificar las librerias necesarias

```sh
$ pip install djangorestframework 3.8.2
$ python manage.py makemigrations
$ python manage.py migrate
$ python manage.py createsuperuser
$ python manage.py runserver
```