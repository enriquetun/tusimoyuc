from django.shortcuts import render
from rest_framework.generics import ListAPIView
from rest_framework.viewsets import ModelViewSet
from rest_framework.renderers import JSONRenderer, BrowsableAPIRenderer
from django.db.models import Q
from django.contrib.gis.geos import Point
from django.contrib.gis.measure import Distance

from .models import Hotel, Foto
from .serializers import HotelSerializer, FotoSerializer
# Create your views here.

    # modelo basico de la lista
# class Hoteles(ListAPIView):
#     queryset = Hotel.objects.all()
#     serializer_class = HotelSerializer

class Hoteles(ListAPIView):
    serializer_class = HotelSerializer
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    # queryset = Hotel.objects.all()
    pk = None
    point = None

    def get(self, request, *args, **kwargs):
        if(self.request.GET.get('pk')):
            self.pk = Hotel.objects.filter(pk=self.request.GET.get('pk'))
            return self.list(request, *args, **kwargs)
        
        if (self.request.GET.get('geo_localidad')):
            self.point = func_set_point(self.request)
        
        return self.list(request, *args, **kwargs)
    
    def get_queryset(self):
        """  El objeto Q encapsula una expresión SQL en un objeto Python que se puede usar 
        en operaciones relacionadas con la base de datos."""
        if(self.pk):
            hotel = Hotel.objects.filter(Q(pk__in = self.pk))
            
            return hotel
        
        if(self.point):
            hotelesPoint = Hotel.objects.filter(Q(geo_localidad__pnt__distance_lt=(self.point, Distance(km=1))))

            return hotelesPoint

        else:
            hoteles = Hotel.objects.all()
        
        return hoteles

    """ def get(self, request, *args, **kwargs):
        if(self.request.GET.get('pk')):
            self.pk = Hotel.objects.filter(pk=self.request.GET.get('pk'))
            return self.list(request, *args, **kwargs)
        
        self.list(request, *args, **kwargs)

    def get_queryset(self, *args, **kwargs):
        queryset = Hotel.objects.all()
        nombre = self.request.query_params.get('nombre', None)
        pk = self.request.query_params.get('pk', None)
        geo_localidad = self.request.query_params.get('geo_localida', None)
        
        if pk is not None:
            queryset = queryset.filter(pk=pk)
        if nombre is not None:
            queryset = queryset.filter(nombre=nombre)
        if geo_localidad is not None:
            queryset = queryset.filter(geo_localidad__pnt__distance_lt=(geo_localidad, Distance(km=1)))
        return queryset
    """
class Fotos(ListAPIView):
    # queryset = Foto.objects.all()
    serializer_class = FotoSerializer
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    # hotel = None

    def get_queryset(self, *args, **kwargs):
        queryset = Foto.objects.all()
        pk_hotel = self.request.query_params.get('hotel', None)

        if pk_hotel is not None:
            queryset = queryset.filter(hotel=pk_hotel)
        
        else: 
            queryset = []

        return queryset



def func_set_point(request):
    if (request.GET.get('geo_localidad')):
        geo_localidad = request.GET.get('geo_localidad')
        geo_localidad = geo_localidad.split(',')
        lat = float(geo_localidad[0])
        lng = float(geo_localidad[1])
        point = Point(lng, lat)

        return point


