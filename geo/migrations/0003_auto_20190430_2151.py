# Generated by Django 2.1.1 on 2019-04-30 21:51

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('geo', '0002_foto'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='foto',
            options={'managed': True, 'verbose_name': 'Foto', 'verbose_name_plural': 'Fotos'},
        ),
        migrations.AlterModelOptions(
            name='hotel',
            options={'managed': True, 'verbose_name': 'Hotel', 'verbose_name_plural': 'Hoteles'},
        ),
        migrations.AlterModelTable(
            name='foto',
            table='Foto',
        ),
        migrations.AlterModelTable(
            name='hotel',
            table='Hotel',
        ),
    ]
