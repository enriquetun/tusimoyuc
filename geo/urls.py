from django.urls import path
from .views import Hoteles, Fotos

urlpatterns = [
    path('lista/', Hoteles.as_view()),
    path('fotos/', Fotos.as_view()),
]