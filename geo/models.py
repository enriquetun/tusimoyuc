from django.db import models
from django.contrib.gis.db.models import PointField # <-- Me sirve para los mapas
from django.contrib.gis.geos import Point
# Create your models here.
class Geolocalidad(models.Model):
    latitud = models.CharField(max_length=32)
    longitud = models.CharField(max_length=32)
    pnt = PointField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'Geo_Localidad'
        verbose_name = 'Geolocalidad'
        verbose_name_plural = 'Geolocalidades'
    
    def __str__(self):
        return '%s %s' % (self.latitud, self.longitud)
    
    def save(self, *args, **kwargs):
        self.pnt = Point(float(self.longitud), float(self.latitud))
        super().save(*args, **kwargs)


class Hotel(models.Model):
    nombre = models.CharField(max_length=256, blank=True, null=True)
    geo_localidad = models.ForeignKey('Geolocalidad', blank=True, null=True, on_delete=models.CASCADE)

    class Meta:
        managed = True
        db_table = 'Hotel'
        verbose_name = 'Hotel'
        verbose_name_plural = 'Hoteles'

    # def __str__(self):
    #     return '%s %s' % (self.geo_localidad,)


class Foto(models.Model):
    hotel = models.ForeignKey('Hotel', default=None, on_delete=models.CASCADE)
    src = models.ImageField(verbose_name="Imagen", upload_to="projects")
    fecha_de_registro = models.DateTimeField(auto_now_add=True)

    class Meta:
        managed = True
        db_table = 'Foto'
        verbose_name = 'Foto'
        verbose_name_plural = 'Fotos'

    # def __str__(self):
    #     return '%s %s' % (self.src)