from django.contrib import admin

from .models import Geolocalidad, Hotel, Foto
# Register your models here.


admin.site.register(Geolocalidad)
admin.site.register(Hotel)
admin.site.register(Foto)