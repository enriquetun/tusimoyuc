from rest_framework import serializers
from .models import Geolocalidad, Hotel, Foto

class GeolocalidadSerializer(serializers.ModelSerializer):
    class Meta: 
        model = Geolocalidad
        fields = ('latitud', 'longitud')

class HotelSerializer(serializers.ModelSerializer):
    geo_localidad = GeolocalidadSerializer()

    class Meta:
        model = Hotel
        fields = ('pk', 'nombre', 'geo_localidad')
    
class FotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Foto
        fields = ('pk', 'hotel', 'src', 'fecha_de_registro')